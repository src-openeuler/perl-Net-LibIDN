%define mod_name Net-LibIDN

Name:           perl-Net-LibIDN
Version:        0.12
Release:        35
Summary:        Perl bindings for GNU Libidn
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/%{mod_name}
Source0:        https://cpan.metacpan.org/authors/id/T/TH/THOR/%{mod_name}-%{version}.tar.gz
Patch0:         Net-LibIDN-0.12-Respect-Config-s-cc-ccflags-and-ldflags.patch

BuildRequires:  gcc libidn-devel perl-interpreter perl-devel perl-generators
BuildRequires:  perl(ExtUtils::MakeMaker) perl(Getopt::Long) perl(AutoLoader)
BuildRequires:  perl(Carp) perl(Exporter) perl(Test)

%{?perl_default_filter}

%description
Provides bindings for GNU Libidn, a C library for handling Internationalized
Domain Names according to IDNA (RFC 3490), in a way very much inspired by
Turbo Fredriksson's PHP-IDN.

%package_help

%prep
%autosetup -n %{mod_name}-%{version} -p1

for F in _LibIDN.pm; do
    iconv -f latin1 -t utf-8 < "$F" > "${F}.utf"
    sed -i -e '/^=encoding\s/ s/latin1/utf-8/' "${F}.utf"
    touch -r "$F" "${F}.utf"
    mv "${F}.utf" "$F"
done;

%build
perl Makefile.PL NO_PACKLIST=1 INSTALLDIRS=vendor
%make_build

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%files
%{perl_vendorarch}/Net
%{perl_vendorarch}/auto/Net

%files help
%doc Artistic Changes README
%{_mandir}/man3/Net::LibIDN.3*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 0.12-35
- drop useless perl(:MODULE_COMPAT) requirement

* Tue Oct 25 2022 jiangchuangang <jiangchuangang@huawei.com> - 0.12-34
- define mod_name to opitomize the specfile

* Mon Feb 17 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.12-33
- Package init
